import os

normDir = 'multilexnorm/test-eval/test/intrinsic_evaluation/'

os.system('mkdir -p cl-monoisepreds/intrinsic_evaluation/')
os.system('mkdir -p cl-monoisepreds/extrinsic_evaluation/')

def getScore(path):
    for line in open(path):
        if line.startswith('ERR: '):
            if 'e' in line: # because of error in conversion of tiny numbers
                return 0.0
            return float(line.strip().split(' ')[-1])
    return 0.0

for lookup in [False, True]:
    bests = {}
    langs = sorted(os.listdir(normDir))
    print(' & '.join([''] + [lang.upper() for lang in langs]) + ' \\\\')
    for tgtLang in langs:
        row = [tgtLang.upper()]
        for srcLang in langs:
            if srcLang == tgtLang:
                row.append('--')
            else:
                if lookup:
                    path = 'preds/noLookup.' + srcLang + '-' + tgtLang + '.eval'
                else:
                    path = 'preds/' + srcLang + '-' + tgtLang + '.eval'
                row.append('{:.2f}'.format(getScore(path)*100))
        highest = max([0.0 if x == '--' else float(x) for x in row[1:]])
        for i in range(len(row)):
            if row[i] == str(highest):
                row[i] = '\\textbf{' + row[i] + '}'
        print(' & '.join(row) + ' \\\\')

