import os

normDir = 'multilexnorm/test-eval/test/intrinsic_evaluation/'

os.system('mkdir -p machamppreds/intrinsic_evaluation/')
os.system('mkdir -p machamppreds/extrinsic_evaluation/')


for lang in sorted(os.listdir(normDir)):
    cmd = 'python3 predict.py logs/xlmr.' + lang + '/*/model.tar.gz ../' + normDir + lang + '/test.norm.masked ' 
    os.system('mkdir -p machamppreds/intrinsic_evaluation/' + lang)
    cmd += ' ../machamppreds/intrinsic_evaluation/' + lang + '/test.norm.pred'
    print(cmd)

for treebank in os.listdir('multilexnorm/test-eval/test/extrinsic_evaluation/'):
    lang = treebank.split('-')[1]
    cmd = 'python3 predict.py logs/xlmr.' + lang + '/*/model.tar.gz ../multilexnorm/test-eval/test/extrinsic_evaluation/' + treebank 
    cmd += ' ../machamppreds/extrinsic_evaluation/' + treebank.replace('masked', 'pred')
    print(cmd)
    



