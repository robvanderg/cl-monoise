import os

normDir = 'multilexnorm/test-eval/test/intrinsic_evaluation/'

for lang in os.listdir(normDir):
    cmd = './tmp/bin/binary -m TR ' + ' -f 110111111111 '
    if os.path.isfile('multilexnorm/data/' + lang + '/dev.norm'):
        os.system('cat multilexnorm/data/' + lang + '/train.norm multilexnorm/data/' + lang + '/dev.norm > multilexnorm/data/' + lang + '/traindev.norm')
        train = '../../multilexnorm/data/' + lang + '/traindev.norm'
    else:
        train = '../../multilexnorm/data/' + lang + '/train.norm'
    cmd += ' -i ' + train
    if lang in ['da', 'de', 'it', 'nl', 'tr', 'trde']:
        cmd += ' -C'
    cmd += ' -n 8 -b '
    #cmd += ' -r ../working/full' + lang
    #cmd += ' -n 8'
    cmd += ' -r ../working/noLookup.full' + lang + ''
    cmd += ' -d ../../monoise/data/' + lang
    print('cd monoise/src && ' + cmd + ' && cd ../../')
    if len(lang) > 2: # codeswitch
        cmd = 'cd csmonoise/src && ' + cmd.replace('monoise/data/' + lang, 'monoise/data/' + lang[2:])
        cmd += ' -2 ../../monoise/data/' + lang[:2]
        print(cmd + ' && cd ../../')
