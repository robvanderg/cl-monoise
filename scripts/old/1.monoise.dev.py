import os
normDir = 'multilexnorm/dev-eval/dev/intrinsic_evaluation/'

for lang in os.listdir(normDir):
    for bad in [True, False]:
        cmd = './tmp/bin/binary -m TR -i ../../multilexnorm/data/' + lang + '/train.norm'
        cmd += ' -d ../../monoise/data/' + lang + ' -f 110111111111'
        if os.path.isfile('multilexnorm/data/' + lang + '/dev.norm'):
            cmd += ' -D ../../multilexnorm/data/' + lang + '/dev.norm'
        if lang in ['da', 'de', 'it', 'nl', 'tr', 'trde']:
            cmd += ' -C'
        if bad:
            cmd += ' -n 8 -b'
            cmd += ' -r ../working/noLookup.' + lang
        else:
            cmd += ' -n 8'
            cmd += ' -r ../working/noLookup.' + lang + '.notbad'
        if len(lang) > 2: # codeswitch
            cmd2 = 'cd csmonoise/src && ' + cmd.replace('../../monoise/data/' + lang, '../../monoise/data/' + lang[:2]) + ' -2 ../../monoise/data/' + lang[2:]
            if bad and not os.path.isfile('csmonoise/working/noLookup.' + lang + '.forest'):
                print(cmd2 + ' && cd ../../')
            if not bad and not os.path.isfile('csmonoise/working/noLookup.' + lang + '.notbad.forest'):
                print(cmd2 + ' && cd ../../')
        cmd2 = 'cd monoise/src && ' + cmd
        if bad and not os.path.isfile('monoise/working/noLookup.' + lang + '.forest'):
            print(cmd2 + ' && cd ../../')
        if not bad and not os.path.isfile('monoise/working/noLookup.' + lang + '.notbad.forest'):
            print(cmd2 + ' && cd ../../')
