import os

normDir = 'multilexnorm/dev-eval/dev/intrinsic_evaluation/'

for lang in os.listdir(normDir):
    for bad in [True,False]:
        if not os.path.isfile('multilexnorm/data/' + lang + '/dev.norm'):
            continue
        cmd = './tmp/bin/binary -m TE -i ../../multilexnorm/data/' + lang + '/dev.norm'
        cmd += ' -W -d ../../monoise/data/' + lang
        if lang in ['da', 'de', 'it', 'nl', 'tr', 'trde']:
            cmd += ' -C'
        if bad:
            cmd += ' -n 8 -b'
            cmd += ' -r ../working/' + lang
            out = '../working/' + lang  + '.eval'
        else:
            cmd += ' -n 8'
            cmd += ' -r ../working/' + lang + '.notbad'
            out = '../working/' + lang + '.notbad.eval'
        cmd2 = 'cd monoise/src && ' + cmd + ' > ' + out
        print(cmd2 + ' && cd ../../')
        if len(lang) > 2: # codeswitch
            cmd2 = 'cd csmonoise/src && ' + cmd.replace('../../monoise/data/' + lang, '../../monoise/data/' + lang[:2]) + ' -2 ../../monoise/data/' + lang[2:]
            print(cmd2 + ' > ' + out + ' && cd ../../')
