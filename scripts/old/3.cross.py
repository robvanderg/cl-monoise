import os

normDir = 'multilexnorm/test-eval/test/intrinsic_evaluation/'

os.system('mkdir -p preds')

for srcLang in os.listdir(normDir):
    for tgtLang in os.listdir(normDir):
        if srcLang == tgtLang:
            continue
        cmd = './tmp/bin/binary -m TE -i ../../multilexnorm/data/' + tgtLang + '/train.norm'
        cmd += ' -W -d ../data/' + tgtLang
        if tgtLang in ['da', 'de', 'it', 'nl', 'tr', 'trde']:
            cmd += ' -C'
        if srcLang not in ['hr', 'iden', 'sl']:
            cmd += ' -b '
        cmd += ' -n 8'
        if os.path.isfile('monoise/working/' + srcLang + '.forest'):
            cmd += ' -r ../working/noLookup.' + srcLang
        elif os.path.isfile('monoise/working/full' + srcLang + '.forest'):
            cmd += ' -r ../working/noLookup.full' + srcLang
        else:
            print("model not found!" + srcLang)
            continue
        out = '../../preds/' + srcLang + '-' + tgtLang + '.eval'
        #if len(srcLang) > 2: # codeswitch
        #    cmd2 = 'cd csmonoise/src && ' + cmd.replace('../data/' + srcLang, '../../monoise/data/' + srcLang[:2]) + ' -2 ../../monoise/data/' + srcLang[2:]
        #    print(cmd2 + ' > ' + out + ' && cd ../../')
        #else:
        cmd2 = 'cd monoise/src && ' + cmd + ' > ' + out
        print(cmd2 + ' && cd ../../')

