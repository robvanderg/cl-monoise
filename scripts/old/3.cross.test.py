import os

normDir = '../multilexnorm/test-eval/test/intrinsic_evaluation/'

os.system('mkdir -p cl-monoisepreds/intrinsic_evaluation/')
os.system('mkdir -p cl-monoisepreds/extrinsic_evaluation/')

def getScore(path):
    for line in open(path):
        if line.startswith('ERR'):
            if 'e' in line: # because of error in conversion of tiny numbers
                return 0.0
            return float(line.strip().split(' ')[-1])
    return 0.0

bests = {}
langs = sorted(os.listdir(normDir))
for tgtLang in langs:
    scores = []
    for srcLang in langs:
        if srcLang == tgtLang:
            scores.append((0.0, srcLang))
        else:
            scores.append((getScore('preds/' + srcLang + '-' + tgtLang + '.eval'), srcLang))
    best = sorted(scores)[-1]
    #print(tgtLang, best)
    bestLang = best[1]
    bests[tgtLang] = bestLang
    cmd = 'cd monoise/src && ./tmp/bin/binary -m RU -W -i ../../' + normDir + tgtLang + '/test.norm.masked -W'
    if tgtLang in ['da', 'de', 'it', 'nl', 'tr', 'trde']:
        cmd += ' -C'
    if bestLang not in ['hr', 'iden', 'sl']:
        cmd += ' -b '
    cmd += ' -d ../data/' + tgtLang
    if os.path.isfile('monoise/working/noLookup.' + bestLang + '.forest'):
        cmd += ' -r ../working/noLookup.' + bestLang
    elif os.path.isfile('monoise/working/noLookup.full' + bestLang + '.forest'):
        cmd += ' -r ../working/noLookup.full' + bestLang
    os.system('mkdir -p cl-monoisepreds/intrinsic_evaluation/' + tgtLang)
    cmd += ' > ../../cl-monoisepreds/intrinsic_evaluation/' + tgtLang + '/test.norm.pred'
    print(cmd + ' && cd ../../ ')

for tgtTreebank in os.listdir('../multilexnorm/test-eval/test/extrinsic_evaluation/'):
    tgtLang = tgtTreebank.split('-')[1]
    bestLang = bests[tgtLang]
    cmd = 'cd monoise/src && ./tmp/bin/binary -m RU -W -i ' + '../../multilexnorm/test-eval/test/extrinsic_evaluation/' + tgtTreebank + ' -W'
    if tgtLang in ['da', 'de', 'it', 'nl', 'tr', 'trde']:
        cmd += ' -C'
    if bestLang not in ['hr', 'iden', 'sl']:
        cmd += ' -b '
    cmd += ' -d ../data/' + tgtLang
    if os.path.isfile('monoise/working/noLookup.' + bestLang + '.forest'):
        cmd += ' -r ../working/noLookup.' + bestLang
    elif os.path.isfile('monoise/working/noLookup.full' + bestLang + '.forest'):
        cmd += ' -r ../working/noLookup.full' + bestLang
    cmd += ' > ../../cl-monoisepreds/extrinsic_evaluation/' + tgtTreebank.replace('masked', 'pred')
    print(cmd + ' && cd ../../ ')
    



