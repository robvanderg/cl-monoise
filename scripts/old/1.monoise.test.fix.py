import os

for treebankMasked in os.listdir('multilexnorm/test-eval/test/extrinsic_evaluation/'):
    treebankPred = treebankMasked.replace('norm.masked', 'norm.pred')
    cmd = 'cut -f 2 monoisepreds/extrinsic_evaluation/' + treebankPred + ' > 2'
    os.system(cmd)
    cmd = 'cut -f 1 multilexnorm/test-eval/test/extrinsic_evaluation/' + treebankMasked + ' > 1'
    os.system(cmd)
    cmd = 'paste 1 2 | sed "s;^	$;;g" > monoisepreds/extrinsic_evaluation/' + treebankPred
    os.system(cmd)

    
