import os

normDir = 'multilexnorm/test-eval/test/intrinsic_evaluation/'

os.system('mkdir -p monoisepreds/intrinsic_evaluation/')
os.system('mkdir -p monoisepreds/extrinsic_evaluation/')

def getScore(path):
    for line in open(path):
        if line.startswith('ERR'):
            if 'e' in line: # because of error in conversion of tiny numbers
                return 0.0
            return float(line.strip().split(' ')[-1])
    return 0.0

langs = sorted(os.listdir(normDir))
for lang in langs:
    cmd = './tmp/bin/binary -m RU -W -i ../../' + normDir + lang + '/test.norm.masked -W'
    if lang in ['da', 'de', 'it', 'nl', 'tr', 'trde']:
        cmd += ' -C'
    cmd += ' -d ../../monoise/data/' + lang[-2:]
    if os.path.isfile('monoise/working/full' + lang + '.forest'):
        cmd += ' -r ../working/full' + lang
    elif os.path.isfile('monoise/working/' + lang + '.forest'):
        cmd += ' -r ../working/' + lang
    if lang in ['hr', 'iden', 'sl']:
        cmd += '.notbad '
    else:
        cmd += ' -b '
        
    os.system('mkdir -p monoisepreds/intrinsic_evaluation/' + lang)
    cmd += ' > ../../monoisepreds/intrinsic_evaluation/' + lang + '/test.norm.pred'
    if len(lang) > 2:
        print('cd csmonoise/src/ && ' + cmd + ' -2 ../../monoise/data/' + lang[:2] + ' && cd ../../ ' )
    else:
        print('cd monoise/src/ && ' + cmd + ' && cd ../../ ')

for tgtTreebank in os.listdir('multilexnorm/test-eval/test/extrinsic_evaluation/'):
    lang = tgtTreebank.split('-')[1]
    cmd = './tmp/bin/binary -m RU -W -i ' + '../../multilexnorm/test-eval/test/extrinsic_evaluation/' + tgtTreebank + ' -W'
    if lang in ['da', 'de', 'it', 'nl', 'tr', 'trde']:
        cmd += ' -C'
    if lang not in ['hr', 'iden', 'sl']:
        cmd += ' -b '
    cmd += ' -d ../../monoise/data/' + lang
    if os.path.isfile('monoise/working/full' + lang + '.forest'):
        cmd += ' -r ../working/full' + lang
    elif os.path.isfile('monoise/working/' + lang + '.forest'):
        cmd += ' -r ../working/' + lang
    cmd += ' > ../../monoisepreds/extrinsic_evaluation/' + tgtTreebank.replace('masked', 'pred')
    print('cd monoise/src/ && ' + cmd + ' && cd ../../ ')

    
    



