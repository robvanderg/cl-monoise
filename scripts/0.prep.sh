# get data
git clone https://robvanderg@bitbucket.org/robvanderg/multilexnorm.git
cd multilexnorm
git reset --hard 526c68d9a5b4b3546e064fec9329c10dd0cf6cfe
cd ../

# get MoNoise
git clone https://robvanderg@bitbucket.org/robvanderg/monoise.git
cd monoise/src
git reset --hard 1dadf1d881d05c05c82a73fe4302f941fc6c9ce2
icmbuild
cd ../
cd data
for lang in da de en es hr id iden it nl sl sr tr trde;
do
    if [ ! -d $lang/aspell-model ]; then
        echo http://www.itu.dk/people/robv/data/monoise/"$lang".tar.gz 
        curl http://www.itu.dk/people/robv/data/monoise/"$lang".tar.gz | tar xvz
    fi
done
cd ../../

# get MaChAmp
git clone https://github.com/machamp-nlp/machamp.git
cd machamp
git reset --hard 66e60b3aa6c6a32f84a2b42b7d3cb2459d3f9f1f
cd ../


