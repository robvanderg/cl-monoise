import os

dataDir = 'multilexnorm/data/'
for lang in sorted(os.listdir(dataDir)):
    if os.path.isfile(dataDir + lang + '/dev.norm'):
        for model in ['xlmr', 'mbert']:
            predPath = 'machamp/logs/' + model + '.' + lang + '/' 
            predPath = predPath + os.listdir(predPath)[0] + '/norm.dev.out'

            cmd = 'python3 predict.py logs/' + model + '.' + lang + '/*/model.tar.gz ../' + dataDir + lang + '/dev.norm ' + predPath.replace('machamp/','')
            print(cmd)

    



