import os
import normEval

dataDir = 'multilexnorm/data/'

def getScore(model, lang):
    goldPath = dataDir + lang + '/dev.norm'
    predPath = 'machamp/logs/' + model + '.' + lang + '/'
    predPath = predPath + os.listdir(predPath)[0] + '/norm.dev.out'
    goldRaw, goldNorm = normEval.loadNormData(goldPath)
    predRaw, predNorm = normEval.loadNormData(predPath)
    lai, acc, err = normEval.evaluate(goldRaw, goldNorm, predNorm, True, False)
    return '{:.2f}'.format(err*100)


for lang in sorted(os.listdir(dataDir)):
    if os.path.isfile(dataDir + lang + '/dev.norm'):
        bertScore = getScore('mbert', lang)
        xlmScore = getScore('xlmr', lang)
        print(' & '.join([lang, bertScore, xlmScore]) + '\\\\')

