./scripts/0.prep.sh

python3 scripts/1.monoise.train.py > 1.train.sh
chmod +x 1.train.sh
./1.train.sh

python3 scripts/1.monoise.dev.py > 1.dev.sh
chmod +x 1.dev.sh
./1.dev.sh

python3 scripts/1.monoise.test.py > 1.test.sh
chmod +x 1.test.sh
./1.test.sh

python3 scripts/1.monoise.testFix.py

python3 scripts/2.machamp.train.py > 2.train.sh
chmod +x 2.train.sh
./2.train.sh

python3 scripts/2.machamp.devPred.py > 2.devPred.sh
chmod +x 2.devPred.sh
2.devPred.sh

python3 scripts/2.machamp.pred.py > 2.pred.sh
chmod +x 2.pred.sh
2.pred.sh

