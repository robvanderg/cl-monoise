import os
from allennlp.common import Params

normDir = 'multilexnorm/test-eval/test/intrinsic_evaluation'

def makeConfig(lang):
    data = {}
    data['word_idx'] = 0
    data['tasks'] = {'norm':{}}
    data['tasks']['norm']['task_type']='string2string'
    data['tasks']['norm']['column_idx']=1
    data['train_data_path'] = '../multilexnorm/data/' + lang + '/train.norm'
    if os.path.isfile('multilexnorm/data/' + lang + '/dev.norm'):
        data['validation_data_path'] = '../multilexnorm/data/' + lang + '/dev.norm'
    allenConfig = Params({'norm': data})
    allenConfig.to_file('configs/norm-' + lang + '.json')
    return '../configs/norm-' + lang + '.json'

for lang in os.listdir(normDir):
    configPath = makeConfig(lang)
    cmd = 'python3 train.py --dataset_config ' + configPath + ' --parameters_config ../configs/params-xlmr.json --name ' + 'xlmr.' + lang
    print(cmd)
    cmd = 'python3 train.py --dataset_config ' + configPath + ' --name ' + 'mbert.' + lang
    print(cmd)
