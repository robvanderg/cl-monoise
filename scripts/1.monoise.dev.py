import os

normDir = 'multilexnorm/test-eval/test/intrinsic_evaluation/'

os.system('mkdir -p preds')

for srcLang in os.listdir(normDir):
    for tgtLang in os.listdir(normDir):
        for noLookup in [True, False]:
            if srcLang == tgtLang:
                continue

            name = srcLang + '.'
            if noLookup:
                name += 'noLookup.'
            if srcLang not in ['hr', 'iden', 'sl']:
                name += 'bad.'
            name = name[:-1]
            if not os.path.isfile('monoise/working/' + name + '.forest'):
                print('model ' + name + ' not found')
                continue

            dataPath = '../data/' + tgtLang 
            inputPath = '../../multilexnorm/data/' + tgtLang + '/train.norm'
            capString = ' -C ' if tgtLang in ['da', 'de', 'it', 'nl', 'tr', 'trde'] else ''
            lookupString = '-f 110111111111 ' if noLookup else ''
            badString = '' if srcLang in ['hr', 'iden', 'sl'] else '-b '
            out = '../../preds/' + ('noLookup.' if noLookup else '') + srcLang + '-' + tgtLang + '.eval'

            cmd = 'cd monoise/src && ./tmp/bin/binary -m TE -i ' + inputPath + ' -d ' + dataPath + ' -r ../working/' + name
            cmd += ' ' + capString + lookupString + badString + ' > ' + out + ' && cd ../../'
            print(cmd)

