import os

normDir = '../multilexnorm/test-eval/test/intrinsic_evaluation/'

os.system('mkdir -p cl-monoisepreds/intrinsic_evaluation/')
os.system('mkdir -p cl-monoisepreds/extrinsic_evaluation/')

def getScore(path):
    for line in open(path):
        if line.startswith('ERR: '):
            return float(line.strip().split(' ')[-1])
    return 0.0

# find best transfer language
bests = {}
langs = sorted(os.listdir(normDir))
for tgtLang in langs:
    scores = []
    for srcLang in langs:
        if srcLang == tgtLang:
            scores.append((0.0, srcLang))
        else:
            scores.append((getScore('preds/noLookup.' + srcLang + '-' + tgtLang + '.eval'), srcLang))
    best = sorted(scores)[-1]
    #print(tgtLang, best)
    bestLang = best[1]
    bests[tgtLang] = bestLang

# predict for intrinsic eval
for tgtLang in langs:
    srcLang = bests[tgtLang]
    capString = ' -C ' if tgtLang in ['da', 'de', 'it', 'nl', 'tr', 'trde'] else ''
    lookupString = '-f 110111111111 ' #if noLookup else ''
    badString = '' if srcLang in ['hr', 'iden', 'sl'] else '-b '
    inputPath = '../../' + normDir + tgtLang + '/test.norm.masked'
    outPath = '../../cl-monoisepreds/intrinsic_evaluation/' + tgtLang + '/test.norm.pred'
    
    cmd = 'cd monoise/src && ./tmp/bin/binary -m RU -W -i ' + inputPath + ' -d ../data/' + tgtLang
    cmd += ' -r ../working/' + srcLang + '.noLookup' + ('' if srcLang in ['hr', 'iden', 'sl'] else '.bad') + ' '
    cmd += capString + lookupString + badString + ' > ' + outPath + ' && cd ../../'
    os.system('mkdir -p cl-monoisepreds/intrinsic_evaluation/' + tgtLang)
    print(cmd)

# predict for extrinsic eval
for tgtTreebank in os.listdir('../multilexnorm/test-eval/test/extrinsic_evaluation/'):
    tgtLang = tgtTreebank.split('-')[1]
    srcLang = bests[tgtLang]
    lookupString = '-f 110111111111 ' #if noLookup else ''
    badString = '' if srcLang in ['hr', 'iden', 'sl'] else '-b '
    capString = ' -C ' #if tgtLang in ['da', 'de', 'it', 'nl', 'tr', 'trde'] else ''
    inputPath = '../../multilexnorm/test-eval/test/extrinsic_evaluation/' + tgtTreebank 
    outPath = '../../cl-monoisepreds/extrinsic_evaluation/' + tgtTreebank.replace('masked', 'pred')

    cmd = 'cd monoise/src && ./tmp/bin/binary -m RU -W -i ' + inputPath
    cmd += capString + lookupString + badString + ' > ' + outPath + ' && cd ../../'
    print(cmd)
    



