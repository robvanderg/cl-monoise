import os
normDir = 'multilexnorm/test-eval/test/intrinsic_evaluation/'

if not os.path.isdir('monoise/working'):
    os.path.mkdir('monoise/working')

for lang in os.listdir(normDir):
    for noLookup in [True, False]:
        name = lang + '.'
        if noLookup:
            name += 'noLookup.'
        if lang not in ['hr', 'iden', 'sl']:
            name += 'bad.'
        name = name[:-1]

        trainPath = '../../multilexnorm/data/' + lang + '/train.norm'
        devPath = '../../multilexnorm/data/' + lang + '/dev.norm'
        dataPath = '../data/' + lang + '/'
        devString = devPath + ' ' if os.path.isfile(devPath[6:]) else ''
        lookupString = '-f 110111111111 ' if noLookup else ''
        badString = '' if lang in ['hr', 'iden', 'sl'] else '-b '
        capString = '-C ' if lang in ['da', 'de', 'it', 'nl', 'tr', 'trde'] else ''

        cmd = 'cd monoise/src && '
        cmd += './tmp/bin/binary -m TR -i ' + trainPath + ' -d ' + dataPath + ' -r ../working/' + name + ' -n 8 '
        cmd += devString + badString + lookupString + capString + ' && cd ../../'
        if not os.path.isfile('monoise/working/' + name + '.forest'):
            print(cmd)
            
        # TODO To get the exact old scores back, you have to concat train+dev
        #if os.path.isfile('multilexnorm/data/' + lang + '/dev.norm')

