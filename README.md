# CL-MoNoise #
CL-MoNoise is a cross-lingual version of [MoNoise](https://bitbucket.org/robvanderg/monoise). In fact the source code is used directly, so this repo only contains the scripts that can be used to re-run the experiments. Details on how CL-MoNoise works can be found in the paper (available soon). 

# Reproduce #
To reproduce the results reported in the paper and the addendum, you can run all command in `scripts/runAll.sh`. For further installation instructions I would like to refer to the [MoNoise](https://bitbucket.org/robvanderg/monoise) code.

